#!/bin/bash

base_dir=$(dirname $(realpath $0))

cd ${base_dir}
mkdir -p third-party
cd third-party


# GDAL
if [ ! -f gdal-2.1.3.tar.gz ]; then
	wget http://download.osgeo.org/gdal/2.1.3/gdal-2.1.3.tar.gz
fi
tar -xvf gdal-2.1.3.tar.gz


# Fonts
if [ ! -f freefont-ttf-20120503.zip ]; then
	wget http://ftp.gnu.org/gnu/freefont/freefont-ttf-20120503.zip
fi
unzip freefont-ttf-20120503.zip


# Natural Earth
mkdir naturalearth
cd naturalearth
if [ ! -f ne_50m_coastline.zip ]; then
	wget http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/physical/ne_50m_coastline.zip
fi
unzip ne_50m_coastline.zip

if [ ! -f ne_50m_admin_0_boundary_lines_land.zip ]; then
	wget http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/cultural/ne_50m_admin_0_boundary_lines_land.zip
fi
unzip ne_50m_admin_0_boundary_lines_land.zip

if [ ! -f HYP_50M_SR_W.zip ]; then
	wget http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/raster/HYP_50M_SR_W.zip
fi
unzip HYP_50M_SR_W.zip
